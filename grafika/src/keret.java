import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
public class keret extends JFrame implements ActionListener{
    int szelesseg=600;
    int hosszusag=500;
    JTextField szovmez=new JTextField(31);
    JButton epikus_gomb=new JButton("epikus gomb");
    JButton epikus_torles=new JButton("epikus torles");
    public keret(){
        super("Ablak nev");
        setSize(szelesseg, hosszusag);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        FlowLayout gr=new FlowLayout();
        setLayout(gr);
        add(epikus_gomb);
        epikus_gomb.addActionListener(this);
        epikus_torles.addActionListener(this);
        add(szovmez);
        add(epikus_torles);
        setResizable(true);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        JButton forras=(JButton)(e.getSource());
        if(forras==epikus_gomb){
            szovmez.setText("Hello world!");

        }else if(forras==epikus_torles){
            szovmez.setText("");

        }


    }
    public static void main(String[] args) {
        keret hello = new keret();
    }


}
